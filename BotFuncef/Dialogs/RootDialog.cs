﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;

namespace BotFuncef.Dialogs
{
  [LuisModel("0a272478-4dce-4c2c-8303-c9c35996118c", "a92e01c615e34adeb07ef5bf237d7116")]
  [Serializable]
  public class RootDialog : LuisDialog<object>
  {
    [LuisIntent("None")]
    public async Task None(IDialogContext context, LuisResult result)
    {
      await context.PostAsync("Eu sou um bot e não consegui resolver seu problema desculpe-me.");
    }

    [LuisIntent("Ajuda")]
    public async Task Ajuda(IDialogContext context, LuisResult result)
    {
      await context.PostAsync("Em que eu posso te ajudar sr ??");
    }

    [LuisIntent("Cumprimento")]
    public async Task Cumprimento(IDialogContext context, LuisResult result)
    {
      await context.PostAsync("Olá tudo bem ? seja bem vindo ");
    }

    [LuisIntent("Temperatura")]
    public async Task Temperatura(IDialogContext context, LuisResult result)
    {
      if (result.Entities.Count == 0)
        await context.PostAsync("Não foi encontrado dados em nossos sistema, tente novamente !");
      else
      {
        await context.PostAsync($"Temperatura de 24ºC em {result.Entities}");
      }
    }
  }
}